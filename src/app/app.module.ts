import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {UserLoginService} from './services/user-login.service';
import {ConfigService} from './ConfigRouterAPI/config.service';
import { HomeComponent } from './home/home.component';
import { RegiterComponent } from './regiter/regiter.component';
import { SpinnerComponent } from './spinner/spinner.component';
import {HttpClientModule} from '@angular/common/http';
import { AccountManagementComponent } from './account-management/account-management.component';
import { StudentManagementComponent } from './student-management/student-management.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegiterComponent,
    SpinnerComponent,
    AccountManagementComponent,
    StudentManagementComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    UserLoginService,
    ConfigService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
