export class User {
  id: string;
  userName: string;
  fullName: string;
  password: string;
  phone: string;
  email: string;
}
