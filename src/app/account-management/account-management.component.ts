import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {User} from '../shared/models/userModel';
import {UserDeleteRequest} from '../shared/request/user-delete-request';

declare var $: any;

@Component({
  selector: 'app-account-management',
  templateUrl: './account-management.component.html',
  styleUrls: ['./account-management.component.css']
})
export class AccountManagementComponent implements OnInit {

  constructor(private  userservice: UserService) { }

  ngOnInit() {
    this.listUserModel = [];
    this.UserModel = new User();
    this.getAll();
  }
  UserModel: User = new User();
  userdeleteRequest: UserDeleteRequest = new UserDeleteRequest();
  listUserModel: User[] = [];
  getAll() {
    debugger;
    this.userservice.GetUser().subscribe((res: any[]) => {
      debugger;
      console.log("getuser",res);
      this.listUserModel = res;
    });
  }
  getByIdUesr(id: string) {
    debugger;
    this.userservice.getByIdUser(id).subscribe((res: any) => {
      this.UserModel = res;
    });

  }
  onDelete(userId: any){
    const confirmResult = confirm('are you sure ');
    if (confirmResult) {
      debugger;
      this.userdeleteRequest.userID = userId;
      this.userservice.DeleteUser(this.userdeleteRequest).subscribe(response => {
          debugger;
          this.getAll();
          this.resetForm();

      });
    }
  }
  resetForm(){
    this.UserModel = new User();
  }
  onAddUser(){
    this.userservice.AddUser(this.UserModel).subscribe((res: any) =>{
      if(res != null){
        alert('them thanh cong');
        this.getAll();
      }
    })
  }
  onUpData(){
    this.userservice.UpDataUser(this.UserModel).subscribe((res: any) => {
      this.UserModel = res;
      console.log("update",res);
      if(res != null){
        alert('UpDate thanh cong');
      }
      this.getAll();
    })
  }

  openFormUpdate(item: User) {
    this.UserModel = item;
  }
}
