import {Component, OnInit} from '@angular/core';
import {UserLoginService} from '../services/user-login.service';
import {Credentials} from '../shared/models/credentials.interface';
import {RouterModule, Router} from '@angular/router';
import {LocalStorage} from '../common/extention';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  ngOnInit() {
  }
  errors;
  credentials: Credentials = new Credentials();

  constructor(private userService: UserLoginService, private router: Router) {
  }

  login({value, valid}: { value: Credentials, valid: boolean }) {
    this.errors = '';
    if (!valid) {
      return;
    }

    this.userService.login(value.UserName, value.password).subscribe(
      result => {
        debugger;
        if (result) {
          console.log('token', result);
          LocalStorage.setCookie('token_jwt', result.token, 5);
          this.router.navigate(['home']);
        }
      },
      error => this.errors = error);
  }

}
