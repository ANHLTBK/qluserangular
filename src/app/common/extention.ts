import {HttpHeaders} from '@angular/common/http';

export class LocalStorage {
  public static getCookie(cname) {
    debugger;
    var name = cname + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }
  public static setCookie(cname, cvalue, exdays) {
    debugger;
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname  + "="+ cvalue + ";" + expires;
  }
  public static Headers(){
    let token = this.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type':  'application/json'
      })
    };
  }
}
