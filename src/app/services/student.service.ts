import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ConfigService} from '../ConfigRouterAPI/config.service';
import {User} from '../shared/models/userModel';
import {UserDeleteRequest} from '../shared/request/user-delete-request';
import {Observable} from 'rxjs';
import {LocalStorage} from '../common/extention';
import {StudentModel} from '../shared/models/studentModel';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  baseUrl = '';
  constructor(private http: HttpClient, private configService: ConfigService) {
    this.baseUrl = configService.getApiURI();
  }
  GetStudent(): Observable<StudentModel[]>{
    let token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type':  'application/json'
      })
    };
    return this.http.get<StudentModel[]>(this.baseUrl+ 'Student/GetAllSudent', httpOptions);
  }
  DeleteStudent(request: UserDeleteRequest): Observable<any>{
    debugger;
    let token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type':  'application/json'
      })
    };
    debugger;
    return this.http.post(this.baseUrl + 'Student/DeleteStudent',request,httpOptions);
  }
  AddStudent(request: StudentModel): Observable<any>{
    let token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(this.baseUrl + 'Student/AddStudent',request,httpOptions);
  }
  UpDataStudent(request: StudentModel): Observable<any>{
    let token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(this.baseUrl + 'Student/UpDateStudent',request, httpOptions);
  }

  getByIdStudent(id: string): Observable<StudentModel>{
    let token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type':  'application/json'
      })
    };
    return this.http.get<StudentModel>(this.baseUrl+ 'Student/GetAllSudent'+ '/' + id , httpOptions);
  }
}
