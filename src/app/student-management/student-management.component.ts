import { Component, OnInit } from '@angular/core';
import {StudentService} from '../services/student.service';
import {StudentModel} from '../shared/models/studentModel';
import {User} from '../shared/models/userModel';

@Component({
  selector: 'app-student-management',
  templateUrl: './student-management.component.html',
  styleUrls: ['./student-management.component.css']
})
export class StudentManagementComponent implements OnInit {

  studentModel: StudentModel = new StudentModel();
  listStudentModels: StudentModel[] = [];

  constructor(private  studentService: StudentService) { }

  ngOnInit() {
    this.listStudentModels = [];
    this.studentModel = new StudentModel();
    this.onGetALL();
  }


  onGetALL(){
    this.studentService.GetStudent().subscribe(res =>{
      console.log("GetListStudent",res);
      this.listStudentModels = res;
      console.log("studentModels",this.listStudentModels)
    })
  }
  onDeleteStudent(id:any){
    const confirmResult = confirm("are you src");
    if(confirmResult){
      this.studentService.DeleteStudent(id).subscribe((res: any) =>{
        this.onGetALL();
      })
    }

  }
  onUpDateStudent(){
    this.studentService.UpDataStudent(this.studentModel).subscribe((res: any) =>{
      this.studentModel = res;
      if(res != null){
        alert("update stuent thanh cong");
      }
      this.onGetALL();
    })
  }
  onAddStudent(){
    this.studentService.AddStudent(this.studentModel).subscribe((res:any) =>{
      if(res != null){
        alert("them thanh cong");
      }
      else {
        alert("them 1 student that bai");
      }
      this.onGetALL();
    })
  }
  resetForm(){
    this.studentModel = new StudentModel();
  }
  openFormUpdate(item: StudentModel) {
    this.studentModel = item;
  }
}
